
//
//  main.swift
//  SmartForce Sudoku solver
//
//  Created by Eddie Draaisma on 21/12/2018.
//  Copyright © 2018 Draaisma. All rights reserved.
//

import Foundation

func printTimeElapsedWhenRunningCode(_ operation: () -> ()) {
    let startTime = CFAbsoluteTimeGetCurrent()
    operation()
    let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
    print("Time elapsed : \(timeElapsed)s")
}

typealias Board = [[Int]]

let rowCount = 9
let colCount = 9
let posCount = rowCount * colCount


func createPeerList() -> [Int: [Int]] {
    var list: [Int: [Int]] = [:]

    for pos in 0 ..< 81 {
        let r = pos / 9
        let c = pos % 9
        for index in 0 ..< 9 {
            var newPos = index * 9 + c
            if newPos != pos { list[pos, default: []].append(newPos) }
            newPos = r * 9 + index
            if newPos != pos { list[pos, default: []].append(newPos) }
        }
        let boxRow = (r / 3) * 3
        let boxCol = (c / 3) * 3
        
        let r1 = boxRow + (r + 1) % 3
        let r2 = boxRow + (r + 2) % 3
        let c1 = boxCol + (c + 1) % 3
        let c2 = boxCol + (c + 2) % 3
        
        list[pos]!.append(r1 * 9 + c1)
        list[pos]!.append(r2 * 9 + c1)
        list[pos]!.append(r1 * 9 + c2)
        list[pos]!.append(r2 * 9 + c2)
    }
    return list
}

let peers = createPeerList()


func removeFromPeers(board: inout Board, pos p: Int, val v: Int) -> [(Int, Int)] {
    var removed: [(Int, Int)] = []
    for peerPos in peers[p]! {
        if (board[peerPos].count > 1) && board[peerPos].contains(v) {
            board[peerPos] = board[peerPos].filter({$0 != v})
            removed.append((peerPos, v))
        }
    }
    return removed
}


func restoreCells(board: inout Board, removedList: [(Int, Int)]) {
    for (pos, v) in removedList {
        board[pos].append(v)
    }
}



func makeBoard(from gameStr: String) -> Board {
    var board = Array(repeating: [1, 2, 3, 4, 5, 6, 7, 8, 9], count: posCount)
    var pos = 0
    for chr in gameStr {
        let num = Int(String(chr)) ?? 0
        if num != 0 {
            board[pos] = [num]
            let _ = removeFromPeers(board: &board, pos: pos, val: num)
        }
        pos += 1
    }
    return board
}


func printBoard(from board: Board) {
    print()
    for row in 0 ..< 9 {
        if [3, 6].contains(row) {
            print(" ------+-------+------")
        }
        print(" ", terminator: "")
        for col in 0 ..< 9 {
            if [3, 6].contains(col) {
                print("| ", terminator: "")
            }
            if board[row * 9 + col].count == 1 {
            print(board[row * colCount + col][0], terminator: " ")
            }
            else { print("0", terminator: " ")}
        }
        print()
    }
    print()
}



func solveBoard(_ board: inout Board) -> Board? {
    var pos = 0, minPos = 81, minvalue = 10
    while pos < 81 {
        let count = board[pos].count
        if (count > 1) && (count < minvalue) {
            minPos = pos
            minvalue = count
        }
        pos += 1
    }
    
    if minPos < 81 {
        pos = minPos
        let vals = board[pos]
        for val in vals {
            let removed = removeFromPeers(board: &board, pos: pos, val: val)
            board[pos] = [val]
            if let board = solveBoard(&board) {
                return board
            }
            restoreCells(board: &board, removedList: removed)
        }
        board[pos] = vals
        return nil
    }
    else {
        return board
    }
}


let grid1 = "003020600900305001001806400008102900700000008006708200002609500800203009005010300"
let hard1 = ".....6....59.....82....8....45........3........6..3.54...325..6.................."
let most  = "061007003092003000000000000008530000000000504500008000040000001000160800600000000"
let grid2 = "4.....8.5.3..........7......2.....6.....8.4......1.......6.3.7.5..2.....1.4......"
let difficult = "800000000003600000070090200050007000000045700000100030001000068008500010090000400"

let diff = "600008940900006100070040000200610000000000200089002000000060005000000030800001600"
let diff2 = "980700000600090800005008090500006080000400300000070002090300700050000010001020004"

let xx = "...8.1..........435............7.8........1...2..3....6......75..34........2..6.."




let game = hard1

printTimeElapsedWhenRunningCode {
    var board = makeBoard(from: game)
    if let _ = solveBoard(&board) {
        printBoard(from: board)
    }
}
